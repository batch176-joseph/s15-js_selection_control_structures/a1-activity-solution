// 1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
// 2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
console.log(`Hello World!`);



// 3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// - If the total of the two numbers is less than 10, add the numbers
// - If the total of the two numbers is 10 - 19, subtract the numbers
// - If the total of the two numbers is 20 - 29 multiply the numbers
// - If the total of the two numbers is greater than or equal to 30, divide the numbers
// 4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
let x = prompt(`Provide a number`);
let y = prompt(`Provide another number`);
let num1 = parseInt(x);
let num2 = parseInt(y);
let sum = (num1 + num2);

if (sum < 10) {
	sum = num1 + num2;
	console.warn(`The sum of the two numbers are: ${sum}`);
} else if (sum >= 10 && sum <= 19) {
	sum = num1 - num2;
	alert(`The diferrence of the two numbers are: ${sum}`);
} else if (sum >= 20 && sum <= 29) {
	sum = num1 * num2;
	alert(`The product of the two numbers are: ${sum}`);
} else if (sum >= 30) {
	sum = num1 / num2;
	alert(`The quotient of the two numbers are: ${sum}`);
}



// 5. Prompt the user for their name and age and print out different alert messages based on the user input:
// -  If the name OR age is blank/null, print the message are you a time traveler?
// -  If the name AND age is not blank, print the message with the user’s name and age.
let name = prompt(`What is your name?`);
let z = prompt(`What is your age?`);
let age = parseInt(z);

/*if ((name == null || name == undefined) || (age == null || age == undefined)) {
	alert(`Are you a time traveler?`);
} else if ((name != null || name != undefined) && (age != null || age != undefined)) {
	alert(`Hello ${name}. Your age is ${age}`);
}*/

if (!name || !age) {
	alert(`Are you a time traveler?`);
} else {
	alert(`Hello ${name}. Your age is ${age}`);
}

// 6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
// - 18 or greater, print an alert message saying You are of legal age.
// - 17 or less, print an alert message saying You are not allowed here.

function isLegalAge (age) {
	age >= 18 ? alert(`You are of legal age`) : alert(`You are not allowed here`);
}

isLegalAge(age);



// 8. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
// - 18 - print the message You are now allowed to party.
// - 21 - print the message You are now part of the adult society.
// - 65 - print the message We thank you for your contribution to society.
// - Any other value - print the message Are you sure you're not an alien?

	switch(age){
		case 18: 
			console.log(`You are now allowed to party.`);
			break;
		case 21:
			console.log(`You are now part of the adult society.`);
			break;
		case 65:
			console.log(`We thank you for your contribution to society.`);
			break;
		default:
			console.log(`Are you sure you're not an alien?`);
			break;

	}

//////Range Switch Case
/*agecheck(age);

function agecheck(){
	switch(true){
		case (age >= 18 && age <= 20): 
			console.log(`You are now allowed to party.`);
			break;
		case (age >= 21 && age <= 64):
			console.log(`You are now part of the adult society.`);
			break;
		case (age >= 65):
			console.log(`We thank you for your contribution to society.`);
			break;
		default:
			console.log(`Are you sure you're not an alien?`);
			break;

	}
}*/



// 8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.


function ageError (age){

		try {
			alert(isLegalAg(age))
		}

		catch(error){
			console.warn(error.message)
		}
		finally {
			// alert(`Have a good day`)
			isLegalAge(age);
		}
	}

ageError(age);



// 9. Create a git repository named S15.
// 10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 11. Add the link in Boodle.

